# Report a bug

We always want to improve Hog, for this reason your help is very important!

As a Hog user, you are very welcome to report bugs and suggest new features opening a new issue in our Gitlab repository: https://gitlab.cern.ch/hog/Hog/issues

When you report bugs, please do it one at the time. Before reporting, it is very important to check if the same bug has been already reported. Take a look at the list of the reported bugs [here at this link.](https://gitlab.cern.ch/hog/Hog/-/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=Problem%20Report)

Open an issue using the label ![probem report](./figures/problem.png)

If your report is clear and effective, then the chances to have it fixed are higher.
To help us debugging, in the [Description] section of the [Issue] try to describe in detail the problem that you encountered.

Let us know: 

- which error/unexpected message you receive
- expected vs actual result
- if your bug is reproducible and how
- which Hog version you are using
- if you are working on Windows or Linux
- if you are using Vivado or Quartus and which version
- the link to your repository (or to where your code is online stored)

If you want to ask a question about Hog rather than report a bug you can use the label ![question](./figures/question.png)

Please do not use any other labels apart form "Feature proposal", "Question" and "Problem report" because they are meant for developers rather than users.