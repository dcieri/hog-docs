# Beta Release v1.0.8
https://gitlab.cern.ch/api/v4/projects/74736/jobs/artifacts/refs/merge-requests%2F128%2Fhead/raw/changelog.md?job=changelog
## Repository info
- Merge request number: 128
- Branch name: hotfix-ipbus-xml-version

## Changelog

- bugfix xml SHA and Version all zeroes in xml file
