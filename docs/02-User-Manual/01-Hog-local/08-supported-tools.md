# List of supported tools

Below you can find a list of supported tools.

The Hog team does NOT provide access to the listed tools and it does NOT grants any help on the usage of the tools.

The list is intended as a list of tools you can use in an Hog compatible project and be sure The Hog style CI will be able to run on your project.
The following list is not exclusive, meaning other tools might be used in your project and still work with the Hog CI scripts.

- Vivado (earliest tested: <2017.1> latest tested: <2019.2>)
- QuestaSim[^1] (earliest tested: <10.7a> latest tested: <2019.2>)
- doxygen (earliest tested: <1.8.17> latest tested: <1.8.17>)
- git (earliest tested: <2.7.0> latest tested: <2.26.2>)

[^1]: Depends on Vivado version used. Check the compatibility list in: https://www.xilinx.com/support/answers/68324.html
